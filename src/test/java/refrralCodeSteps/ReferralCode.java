package refrralCodeSteps;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class ReferralCode
{
	WebDriver driver;
	boolean test=false;
	//String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE1OCwib3JkZXItYnkiOltbImRlc2MiLFsiZmllbGQtaWQiLDc3MF1dXSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTkyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsNzUxXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBDb252ZXJ0ZWRyZWZlcnJhbCIsWyJmaWVsZC1pZCIsMzU0XV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBDb252ZXJ0ZWRyZWZlcnJhbCJ9LHsiZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoyOSwiY29uZGl0aW9uIjpbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIENvbnZlcnRlZHJlZmVycmFsIixbImZpZWxkLWlkIiwzNTRdXSxbImpvaW5lZC1maWVsZCIsIlJlZmVycmVkIEJ5IixbImZpZWxkLWlkIiwyMjU2XV1dLCJhbGlhcyI6IlJlZmVycmVkIEJ5In1dLCJmaWx0ZXIiOlsiYW5kIixbInRpbWUtaW50ZXJ2YWwiLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIENvbnZlcnRlZHJlZmVycmFsIixbImZpZWxkLWlkIiwzNTVdXSwtMSwiZGF5Iix7fV0sWyJjb250YWlucyIsWyJqb2luZWQtZmllbGQiLCJSZWZlcnJlZCBCeSIsWyJmaWVsZC1pZCIsMjI0OV1dLCJSZWZlcnJhbCBCb251cyIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE1OCwib3JkZXItYnkiOltbImRlc2MiLFsiZmllbGQtaWQiLDc3MF1dXSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTkyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsNzUxXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBDb252ZXJ0ZWRyZWZlcnJhbCIsWyJmaWVsZC1pZCIsMzU0XV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBDb252ZXJ0ZWRyZWZlcnJhbCJ9LHsiZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoyOSwiY29uZGl0aW9uIjpbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIENvbnZlcnRlZHJlZmVycmFsIixbImZpZWxkLWlkIiwzNTRdXSxbImpvaW5lZC1maWVsZCIsIlJlZmVycmVkIEJ5IixbImZpZWxkLWlkIiwyMjU2XV1dLCJhbGlhcyI6IlJlZmVycmVkIEJ5In1dLCJmaWx0ZXIiOlsiYW5kIixbImNvbnRhaW5zIixbImpvaW5lZC1maWVsZCIsIlJlZmVycmVkIEJ5IixbImZpZWxkLWlkIiwyMjQ5XV0sIlJlZmVycmFsIEJvbnVzIix7ImNhc2Utc2Vuc2l0aXZlIjpmYWxzZX1dLFsidGltZS1pbnRlcnZhbCIsWyJqb2luZWQtZmllbGQiLCJSZWZlcnJlZCBCeSIsWyJmaWVsZC1pZCIsMjI1N11dLC0xLCJkYXkiLHt9XV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	public ReferralCode(WebDriver driver) throws InterruptedException, IOException
	{
		driver.get(Url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(35000);
		String error=null;
		try {
			error=(driver.findElement(By.xpath("//*[ contains (text(), 'No results!' ) ]")).getText().toString());
		} catch(Exception e) {}

		if(error != null)
		{

			System.out.println(error);
		} 

		else {
			test = true;
			// Date format for screenshot file name
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// coppy screenshot file into screenshot folder.
			FileUtils.copyFile(file,
					new File("./Referral/" + "Referral" + " " + "(" + " " + fileName + " " + ")" + ".jpg"));		
			System.out.println("hello data are not found");
		}

		Assert.assertTrue(test, "Data Not Found");
//		String count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
//		System.out.println("Text is:"+count);
//		try {
//			
//			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
//			Date date = new Date();
//			String fileName = df.format(date);
//			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//			FileUtils.copyFile(file,
//					new File("./Referral/" + "Referral" + " " + "(" + " " + fileName + " " + ")" + ".jpg"));
//		} catch (Exception e) {
//		}   



	}
}
