package main;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import refrralCodeSteps.Login;
import refrralCodeSteps.ReferralCode;
import webCapability.Capability;

public class MainClass extends Capability
{
	WebDriver driver;
	@BeforeTest
	public void openchrome()
	{
		driver = WebCapability();
		driver.manage().window().maximize();
		
	}
	@Test(priority = 1)
	public void login() throws InterruptedException
	{
		new Login(driver);
	}
	@Test(priority = 2)
	public void referral() throws InterruptedException, IOException
	{
		new ReferralCode(driver);
	}
	@AfterTest
	public void CloseBrowser()
	{

		driver.quit	();
	}
}
